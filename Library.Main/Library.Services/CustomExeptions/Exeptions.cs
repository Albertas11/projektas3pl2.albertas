﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.CustomExeptions
{
    [Serializable]
    public class FailedSaveBookReaderExeption : Exception
    {
        public FailedSaveBookReaderExeption(Exception innerExeption = null)
            : base("Failed save book reader.", innerExeption)
        {

        }
    }

    [Serializable]
    public class InvalidPhoneNumberException : Exception
    {
        public InvalidPhoneNumberException()
            : base("Phone number is not valid.")
        {

        }
    }
}
