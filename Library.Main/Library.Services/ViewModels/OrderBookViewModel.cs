﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class OrderBookViewModel
    {
        public string Name { get; set; }

        public DateTime TakeDate { get; set; }

        public DateTime ReturnDate { get; set; }
    }
}
