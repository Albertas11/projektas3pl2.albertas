﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Library.Services
{
    public class DbConnection
    {
        protected MySqlConnection libraryConnection { get; private set; }

        public DbConnection()
        {
            libraryConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["libraryConnectionString"].ConnectionString);
        }
    }
}
