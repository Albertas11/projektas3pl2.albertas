﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Services;
using Library.ViewModels;

namespace Library.Main
{
    public partial class BookSave : Form
    {
        private AuthorService _authorService;
        private BookViewModel _book;

        public BookSave(BookViewModel book = null)
        {
            _authorService = new AuthorService();
            InitializeComponent();
            PrepareForm();
            if (book != null) {
                FillForm(book);
            }
            else {
                _book = new BookViewModel
                {
                    BookAuthores = new List<AuthorViewModel>()
                };
            }
        }

        private void PrepareForm()
        {
            listBoxAuthores.DataSource = new BindingSource(_authorService.GetAuthores(), null);
            listBoxAuthores.DisplayMember = "Fullname";
            listBoxAuthores.ValueMember = "Id";
            listBoxAuthores.SelectedIndex = -1;

            List<int> asd = new List<int>();
            for (int i = 1547; i <= DateTime.Now.Year; i++)
            {
                asd.Add(i);
            }

            cmbxReleaseDate.DataSource = new BindingSource(asd.OrderByDescending(x => x), null);
        }

        private void FillForm(BookViewModel book)
        {
            _book = book;
            txtBookId.Text = book.BookId.ToString();
            txtName.Text = book.Name;
            cmbxReleaseDate.SelectedItem = book.ReleaseDate;
            foreach (var author in book.BookAuthores)
            {
                for (int i = 0; i < listBoxAuthores.Items.Count; i++)
                {
                    if (((AuthorViewModel)listBoxAuthores.Items[i]).Id == author.Id)
                    {
                        listBoxAuthores.SelectedIndex = i;
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _book.BookAuthores.Clear();
            _book.Name = txtName.Text;
            _book.ReleaseDate = (int)cmbxReleaseDate.SelectedValue;
            foreach (var selectedAutor in listBoxAuthores.SelectedItems)
            {
                _book.BookAuthores.Add(new AuthorViewModel { Id = ((AuthorViewModel)selectedAutor).Id });
            }
            _authorService.SaveBook(_book);

            this.Close();
        }
    }
}
